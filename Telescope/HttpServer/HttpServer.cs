﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;

namespace DynamicReflector.HttpServer
{
    // HttpListener wrapper
    public class HttpServer : IDisposable
    {
        public const char QueryTypeSeparator = '?';
        public const char QueryKeyValueSeparator = '=';
        public const char QueryParamSeparator = '&';

        public HttpServer(int port)
        {
            InitHttpListener(port);
        }

        public int Port { get; private set; }

        HttpListener _l;
        AutoResetEvent _listenForNextRequest = new System.Threading.AutoResetEvent(false);

        // Initialization of responding for requests
        private void InitHttpListener(int port)
        {
            if (!HttpListener.IsSupported)
            {
                throw new Exception("HttpListener not supported");
            }

            Port = port;

            _l = new HttpListener();

            _l.Prefixes.Add("http://localhost:" + port + "/");

            _l.Start();

            ThreadPool.QueueUserWorkItem((obj) =>
            {
                while (_l.IsListening)
                {
                    _l.BeginGetContext(ListenCallback, _l);
                    _listenForNextRequest.WaitOne();
                }
            });
        }

        private void ListenCallback(IAsyncResult result)
        {
            HttpListener listener = result.AsyncState as HttpListener;
            HttpListenerContext context = null;

            if (listener == null)
                return;

            try
            {
                context = listener.EndGetContext(result);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return;
            }
            finally
            {
                _listenForNextRequest.Set();
            }
            if (context == null)
                return;
            ProcessRequest(context);
        }

        private void ProcessRequest(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;

            string responseString = RaiseRequest(request);
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);

            response.ContentLength64 = buffer.Length;

            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            output.Close();
        }

        // Request from client
        // TODO: multithreading version of response, otherwise multiple simultaneous requests will stuck
        public event RequestHandler Request;
        protected string RaiseRequest(HttpListenerRequest req)
        {
            var h = Request;
            if (h != null)
                return h(req);

            return null;
        }

        // Standard resource disposing
        private bool _disposed = false;
        protected virtual void _Dispose(bool disposing)
        {
            if (_l != null)
                _l.Close();
        }
        public virtual void Dispose()
        {
            _Dispose(true);

            _disposed = true;

            GC.SuppressFinalize(this);
        }
        ~HttpServer()
        {
            if (!_disposed)
                _Dispose(false);

            _disposed = true;
        }
    }

    // Request processing handle
    public delegate string RequestHandler(HttpListenerRequest request);
}
