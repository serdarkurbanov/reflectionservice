﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using DynamicReflector.HttpServer;
using System.IO;
using System.Xml.Linq;

namespace DynamicReflector.Service
{
    // Web server, making requests to Nodes for reflected objects' properties
    // When new Node is connected, it makes respond with port number to it. Node opens it's HttpServer on this port
    public class ReflectorService : IDisposable
    {
        // TODO need to make async requests.

        // Object to send requests to Nodes
        WebClient _c;

        // Object to respond for client's requests
        HttpServer.HttpServer _s;

        // List of ports for active Nodes
        string _nodePortsFile;
        List<int> _nodePorts;

        public ReflectorService()
        {
            InitNodePorts();

            InitHttpClient();

            InitHttpServer();
        }

        // Initialization of ReflectorService
        private void InitNodePorts()
        {
            // Load list of ports of active nodes. Questionable. Maybe, we shouldn't store this list of ports
            _nodePortsFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ReflectorNodes.cfg");

            if (File.Exists(_nodePortsFile))
            {
                LoadPorts(_nodePortsFile);
            }
            else
            {
                _nodePorts = new List<int>();
                SavePorts(_nodePortsFile);
            }
        }
        private void InitHttpClient()
        {
            // Init web client and test availability of nodes
            _c = new WebClient();

            // Response from node
            _c.DownloadStringCompleted += (s, a) =>
            {
                //int port = (int)a.UserState;

                //if (a.Error != null) // node did not response
                //{
                //    // TODO: Handle exception properly
                //    lock (_nodePorts)
                //    {
                //    }

                //    return;
                //}
            };
        }
        private void InitHttpServer()
        {
            _s = new DynamicReflector.HttpServer.HttpServer(20000);

            // Request from client or "ping" from node
            _s.Request += (r) =>
            {
                int lastSeparator = r.Url.AbsoluteUri.LastIndexOf('/') + 1;
                string q = r.Url.AbsoluteUri.Substring(lastSeparator, r.Url.AbsoluteUri.Length - lastSeparator);

                if (string.IsNullOrEmpty(q)) // Query from client
                {
                    return GetReplyForClient(r);
                }
                else // Query from node
                {
                    string[] temp = q.Split(DynamicReflector.HttpServer.HttpServer.QueryTypeSeparator);

                    string qType = temp[0];
                    Dictionary<string, string> qParams = temp.Length == 1 ?
                        new Dictionary<string, string>() :
                        temp[1].Split(DynamicReflector.HttpServer.HttpServer.QueryParamSeparator).Select(x =>
                            {
                                string[] kv = x.Split(DynamicReflector.HttpServer.HttpServer.QueryKeyValueSeparator);

                                return new { K = kv[0], V = kv[1] };
                            }).ToDictionary(x => x.K, y => y.V);

                    return GetReplyForNode(qType, qParams);
                }
            };
        }

        // Response to client's requests
        private string GetReplyForClient(HttpListenerRequest request)
        {
            // Request retransmitted to all active nodes
            List<int> ports = new List<int>(_nodePorts);

            // Requested info in form "port, status (online/offline), XML info from the Node"
            List<System.Tuple<string, string, string>> responses = new List<Tuple<string, string, string>>();

            using (System.Threading.AutoResetEvent s = new AutoResetEvent(false))
            {
                int count = ports.Count;
                object countSync = new object();

                if (count == 0)
                    s.Set();

                for (int i = 0; i < count; i++ )
                {
                    var p = ports[i];

                    System.Threading.ThreadPool.QueueUserWorkItem((obj) =>
                    {
                        try
                        {
                            string response = _c.DownloadString(string.Format("http://localhost:{0}/", p));

                            string state = "ONLINE";

                            responses.Add(new Tuple<string, string, string>(p.ToString(System.Globalization.NumberFormatInfo.InvariantInfo), state, response));
                        }
                        catch (Exception e)
                        {
                            // Node is inaccesible
                            string state = "OFFLINE";

                            responses.Add(new Tuple<string, string, string>(p.ToString(System.Globalization.NumberFormatInfo.InvariantInfo), state, "<NodeInfo/>"));
                        }
                        finally
                        {
                            lock (countSync)
                            {
                                count--;
                                if (count == 0)
                                    s.Set();
                            }
                        }
                    });
                }

                // Wait until all nodes are requested and have returned info
                s.WaitOne();

                return GetReplyForClient(responses);
            }
        }
        private string GetReplyForClient(List<Tuple<string, string, string>> replies)
        {
            XDocument doc = new XDocument();
            XElement root = new XElement("ReflectorServiceInfo");

            foreach (var node in replies)
            {
                root.Add(new XElement("Node",
                    new XElement("Port", node.Item1),
                    new XElement("State", node.Item2),
                    XElement.Parse(node.Item3)));
            }

            doc.Add(root);

            return doc.ToString();
        }

        // Reponse to Node's requests (it pings service and waits for response with port number; also it sends info on creation/destroying HttpServer on its side) 
        private string GetReplyForNode(string type, Dictionary<string, string> param)
        {
            switch(type)
            {
                case "NodePing":
                    return GetReplyForNode_Ping(param.ContainsKey("Port") ? (int?)int.Parse(param["Port"]) : null);
                case "NodePortCreated":
                    return GetReplyForNode_PortCreated(int.Parse(param["Port"]));
                case "NodePortClosed":
                    return GetReplyForNode_PortClosed(int.Parse(param["Port"]));
                default:
                    return "Unidentified query";
            }

        }
        private string GetReplyForNode_Ping(int? port)
        {
            // If client sends ping without a port, server should return number of port to create a http service
            if (port == null)
            {
                // TODO: this should be unionised with GetReplyForNode_PortCreated to ensure that node service is created
                lock (_nodePorts)
                {
                    int newPort = 20001;
                    if (_nodePorts.Count != 0)
                    {
                        newPort = _nodePorts[_nodePorts.Count - 1] + 1;
                    }
                    _nodePorts.Add(newPort);

                    return newPort.ToString(System.Globalization.NumberFormatInfo.InvariantInfo);
                }
            }

            // If client sends ping with a port, it notifies server that it's alive.
            // If server is new, it should add this port to a list
            lock (_nodePorts)
            {
                if (!_nodePorts.Contains(port.Value))
                    _nodePorts.Add(port.Value);
            }

            return "OK";
        }
        private string GetReplyForNode_PortCreated(int port)
        {
            // Server should add this port to a list
            lock (_nodePorts)
            {
                if (!_nodePorts.Contains(port))
                    _nodePorts.Add(port);
            }

            return "OK";
        }
        private string GetReplyForNode_PortClosed(int port)
        {
            // Server should remove this port from a list
            lock (_nodePorts)
            {
                if (_nodePorts.Contains(port))
                    _nodePorts.Remove(port);
            }

            return "OK";
        }

        // Loading and unloading set of ports
        private void LoadPorts(string path)
        {
            _nodePorts = new List<int>();

            //_nodePorts = File.ReadAllText(path).Split('\n').Select(
            //    x =>
            //    {
            //        return int.Parse(x.Trim(' ', '\r', '\t'), System.Globalization.NumberFormatInfo.InvariantInfo);
            //    }).ToList();
        }
        private void SavePorts(string path)
        {
            //File.WriteAllText(path, _nodePorts.Select(x =>
            //    {
            //        return x.ToString(System.Globalization.NumberFormatInfo.InvariantInfo);
            //    }).Aggregate((x, y) => string.Concat(x, '\n', y)));
        }

        // Standard resource disposing
        private bool _disposed = false;
        protected virtual void _Dispose(bool disposing)
        {
            if (_c != null)
                _c.Dispose();

            if (_s != null)
                _s.Dispose();
        }
        public void Dispose()
        {
            if (!_disposed)
                _Dispose(true);

            _disposed = true;

            GC.SuppressFinalize(this);
        }
        ~ReflectorService()
        {
            if (!_disposed)
                _Dispose(false);

            _disposed = true;
        }
    }

    // I planned to use this class for multithreaded request handle
    public class NodeRequest
    {
        public List<int> Ports { get; set; }
        public HttpListenerRequest InitRequest { get; set; }
        public List<string> Responses { get; set; }
    }
}
