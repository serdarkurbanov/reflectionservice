﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestReflectionNodeService
{
    class Program
    {
        static void Main(string[] args)
        {
            DynamicReflector.ReflectorNode.ReflectorNode n = new DynamicReflector.ReflectorNode.ReflectorNode(new TestObject(), "NoName");

            Console.ReadLine();
        }
    }


    public class TestObject
    {
        int i = 64;
        public int I { get { return 90; } }
        public object Obj { get { return new InnerObj(); }}
        Str str = new Str(4);
    }
    public class InnerObj
    {
        public int UUU = 777;
    }
    public struct Str
    {
        public Str(int j)
        {
            i = j;
            dt = DateTime.Now;
        }

        Int32 i;
        DateTime dt;
    }
}
