﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestSemaphore
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 4;
            object countSync = new object();

            System.Threading.AutoResetEvent ae = new System.Threading.AutoResetEvent(false);

            for (int i = 0; i < count; i++)
            {
                System.Threading.ThreadPool.QueueUserWorkItem((obj) =>
                {


                    Console.WriteLine("Thread {0} starts", obj);

                    System.Threading.Thread.Sleep(1000);

                    Console.WriteLine("Thread {0} finishes", obj);   
                    
                    lock (countSync)
                    {
                        count--;

                        if (count == 0)
                        {
                            ae.Set();
                        }
                    }

                }, i);
            }

            ae.WaitOne(10000);

            Console.WriteLine("Main thread finished");

            Console.ReadLine();
        }
    }
}
