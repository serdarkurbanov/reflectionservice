﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml.Linq;
using System.Reflection;

namespace DynamicReflector.ReflectorNode
{
    // Node binds to certain object and reflects it's fields and properties
    public class ReflectorNode : IDisposable
    {
        // Object to send requests to ReflectorService
        WebClient _c;

        // Object to make responses to requests from client
        HttpServer.HttpServer _s;
        object _sSync = new object();

        public ReflectorNode(object obj, string name)
        {
            _obj = obj;
            _name = name;

            InitHttpClient();

            //InitHttpServer();
        }

        // Object to reflect it's fields and properties
        private object _obj;
        // Node's name
        private string _name;

        /*
        In current implementation server-node connection is as follows:
        1. Each Node doesn't create HttpServer object with initialization
        2. After creation, each Node creates WebClient object, which sends ping with given timeout to ReflectorService (port is given manually as 20000 now)
        3. If "OK" response with certain port number is received from ReflectorService, this Node creates HttpServer object, which handles ReflectorService requests
        4. After HttpServer is created, ping to ReflectorService is sent with port number specified in request
        5. If ping to ReflectorService fails, HttpServer object on this Node is destroyed.
         */

        // Initialization of node
        private void InitHttpClient()
        {
            _c = new WebClient();

            // Init ping to server
            InitPingToServer(TimeSpan.FromSeconds(2));
        }
        private void InitHttpServer(int port)
        {
            lock (_sSync)
            {
                _s = new DynamicReflector.HttpServer.HttpServer(port);

                _s.Request += (r) =>
                {
                    return ReflectXML(_obj, _name);
                };
            }
        }
        private void CloseHttpServer()
        {
            lock (_sSync)
            {
                if (_s != null)
                {
                    _s.Dispose();
                    _s = null;
                }
            }
        }

        // Send messages to server
        private void InitPingToServer(TimeSpan timeout)
        {
            new System.Threading.Thread(() =>
            {
                for (; ; )
                {
                    string req = "http://localhost:20000/NodePing";

                    lock (_sSync)
                    {
                        if (_s != null)
                        {
                            req += "?Port=" + _s.Port.ToString(System.Globalization.NumberFormatInfo.InvariantInfo);
                        }
                    }

                    try
                    {
                        string response = _c.DownloadString(req);

                        if (response != "OK")
                        {
                            CloseHttpServer();
                            InitHttpServer(int.Parse(response, System.Globalization.NumberFormatInfo.InvariantInfo));

                            // Notify server on successful creation of node
                            try
                            {
                                _c.DownloadString("http://localhost:20000/NodePortCreated?Port=" + response);
                            }
                            catch(Exception e)
                            {
                                //
                            }
                        }
                        else
                        {
                            // Connection is OK, nothing to do
                        }
                    }
                    catch (Exception e)
                    {
                        // Server is down
                        CloseHttpServer();
                    }


                    System.Threading.Thread.Sleep(timeout);
                }
            }).Start();
        }

        // Reply to server request
        private string ReflectXML(object obj, string name)
        {
            // Create XML with data for reflected object and current process
            XDocument doc = new XDocument();

            XElement root = new XElement("NodeInfo");

            ProcessInfo pi = ProcessInfo.GetProcessInfo();
            root.Add(ProcessInfoToXML(pi));

            ReflectionInfo ri = ReflectionInfo.GetReflectionInfo(obj, name);
            root.Add(ReflectionInfoToXML(ri));

            doc.Add(root);

            return doc.ToString();
        }

        // Creation of XML based on current process information and reflected object information
        private static XElement ReflectionInfoToXML(ReflectionInfo ri)
        {
            return new XElement("ReflectionInfo",
                new XElement("MemberType", ri.MemberType),
                new XElement("Name", ri.Name),
                new XElement("Type", ri.Type.ToString()),
                new XElement("Value", ri.Value),
                new XElement("Fields", ri.Fields.Values.Count == 0 ? null : ri.Fields.Values.Select(x => ReflectionInfoToXML(x))),
                new XElement("Properties", ri.Properties.Values.Count == 0 ? null : ri.Properties.Values.Select(x => ReflectionInfoToXML(x)))
                );
        }
        private static XElement ProcessInfoToXML(ProcessInfo pi)
        {
            return new XElement("ProcessInfo",
                new XElement("StartTime", pi.StartTime),
                new XElement("WorkingDirectory", pi.WorkingDirectory),
                new XElement("FileName", pi.FileName),
                new XElement("User", pi.User),
                new XElement("ThreadCount", pi.ThreadCount),
                new XElement("Domain", pi.Domain),
                new XElement("Args", pi.Args),
                new XElement("ProcessName", pi.ProcessName),
                new XElement("PID", pi.PID),
                new XElement("MachineName", pi.MachineName),
                new XElement("TotalProcessorTime", pi.TotalProcessorTime.ToString("hh\\:mm\\:ss\\.fff", System.Globalization.DateTimeFormatInfo.InvariantInfo)),
                new XElement("TotalTime", pi.TotalTime.ToString("hh\\:mm\\:ss\\.fff", System.Globalization.DateTimeFormatInfo.InvariantInfo)),
                new XElement("ProcessorAvgLoad", pi.ProcessorAvgLoad),
                new XElement("VirtualMemory", pi.VirtualMemory),
                new XElement("MaxVirtualMemory", pi.MaxVirtualMemory),
                new XElement("PhysicalMemory", pi.PhysicalMemory),
                new XElement("MaxPhysicalMemory", pi.MaxPhysicalMemory));
        }

        // Standard resource disposing
        bool _disposed = false;
        public void Dispose()
        {
            if(!_disposed)
                _Dispose();

            GC.SuppressFinalize(this);
        }
        private void _Dispose()
        {
            if (_c != null)
            {
                // Send server NodeClose
                try
                {
                    _c.DownloadString("http://localhost:20000/NodePortClosed");
                }
                catch (Exception ex)
                {
                }

                _c.Dispose();
            }

            if (_s != null)
                _s.Dispose();

            _disposed = true;
        }
        ~ReflectorNode()
        {
            if(!_disposed)
                Dispose();
        }
    }

    public class ProcessInfo
    {
        public DateTime StartTime { get; set; }
        public string WorkingDirectory { get; set; }
        public string FileName { get; set; }
        public string User { get;set;}
        public int ThreadCount { get; set; }
        public string Domain { get; set; }
        public string Args { get; set; }
        public string ProcessName { get; set; }
        public int PID { get; set; }
        public string MachineName { get; set; }
        public TimeSpan TotalProcessorTime { get; set; }
        public TimeSpan TotalTime { get { return DateTime.Now - StartTime; } }
        public double ProcessorAvgLoad { get { return TotalProcessorTime.TotalMilliseconds / (TotalTime.TotalMilliseconds * System.Environment.ProcessorCount); } }

        public long VirtualMemory { get; set; }
        public long MaxVirtualMemory { get; set; }
        public long PhysicalMemory { get; set; }
        public long MaxPhysicalMemory { get; set; }


        public static ProcessInfo GetProcessInfo()
        {
            System.Diagnostics.Process cur = System.Diagnostics.Process.GetCurrentProcess();

            return new ProcessInfo
            {
                StartTime = cur.StartTime,
                WorkingDirectory = cur.StartInfo.WorkingDirectory,
                FileName = cur.StartInfo.FileName,
                User = cur.StartInfo.UserName,
                ThreadCount = cur.Threads.Count,
                Domain = cur.StartInfo.Domain,
                Args = cur.StartInfo.Arguments,
                VirtualMemory = cur.VirtualMemorySize64,
                PhysicalMemory = cur.WorkingSet64,
                PID = cur.Id,
                MachineName = cur.MachineName,
                TotalProcessorTime = cur.TotalProcessorTime,
                ProcessName = cur.ProcessName,
                MaxPhysicalMemory = cur.PeakWorkingSet64,
                MaxVirtualMemory = cur.PeakVirtualMemorySize64
            };
        }
        public override bool Equals(object obj)
        {
            // ProcessInfo сравниваются по имени компа, рабочей директории и ID процесса
            if (obj is ProcessInfo)
            {
                ProcessInfo i = obj as ProcessInfo;

                return i.MachineName == MachineName && i.WorkingDirectory == WorkingDirectory && PID == PID;
            }

            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return 
                (MachineName == null ? 0 : MachineName.GetHashCode()) ^
                (WorkingDirectory == null ? 0 : WorkingDirectory.GetHashCode()) ^
                PID.GetHashCode();
        }
    }
    public class ReflectionInfo
    {
        public const string FieldMemberType = "Field";
        public const string PropertyMemberType = "Property";

        public string MemberType { get; set; }
        public string Name { get; set; }
        public Type Type { get; set; }
        public string Value { get; set; }
        public Dictionary<FieldInfo, ReflectionInfo> Fields { get; set; }
        public Dictionary<PropertyInfo, ReflectionInfo> Properties { get; set; }

        public static ReflectionInfo GetReflectionInfo(object obj, string name)
        {
            // TODO: synchronization, indexed propperties
            if (obj == null)
            {
                return new ReflectionInfo
                {
                    Name = name,
                    Type = typeof(object), 
                    Value = "null",
                    Fields = new Dictionary<FieldInfo, ReflectionInfo>(),
                    Properties = new Dictionary<PropertyInfo, ReflectionInfo>()
                };
            }

            // NOTE: 
            // Inside primitive types there are fields or properties of the same type as current. This leads to infinite recursion when reflecting
            // To avoid this, filter is applied: .Where(x => x.FieldType.IsClass || x.FieldType != obj.GetType())
            // Not to forget check this behavior properly
            // Maybe this filter can make error when reflecting object containing embedded object of the same type
            return new ReflectionInfo
            {
                Name = name,
                Type = obj.GetType(), 
                Value = obj.ToString(), 
                Fields = obj.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).Where(x => x.FieldType.IsClass || x.FieldType != obj.GetType()).ToDictionary(x => x, y => 
                    {
                        var r = GetReflectionInfo(y.GetValue(obj), y.Name);
                        r.MemberType = FieldMemberType;

                        return r;
                    }),
                Properties = obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).Where(x => x.PropertyType.IsClass || x.PropertyType != obj.GetType()).ToDictionary(x => x, y => 
                    {
                        var r = ReflectionInfo.GetReflectionInfo(y.GetValue(obj, null), y.Name);
                        r.MemberType = PropertyMemberType;

                        return r;
                    })
            };
        }
    }
}
